import os

import offlineimap.imaputil as IU


def get_env(env_var):
    return os.environ[env_var]


### custom IMAP tags that need to be synced,
### mapped to letters (Maildir flags),
### found in the last part of the file name ending with ':2,XXX'
if not hasattr(IU, "have_added_custom_tag"):
    IU.flagmap += [
        ("filip", "W"),
        ("glenn", "X"),
        ("giles", "Y"),
        ("conrad", "Z"),
    ]
    IU.have_added_custom_tag = True
