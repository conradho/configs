
package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"
require("common")

local account = get_account()

function merge_folder(account)
    bad_folder_name = 'Archive.2017'
    good_folder_name = 'Archives.2017'

    emails_to_be_moved = account[bad_folder_name]:select_all()
    log_result(emails_to_be_moved, 'emails to be moved')

    log_result(account[good_folder_name]:select_all(), 'folder email pre-merge')
    emails_to_be_moved:move_messages(account[good_folder_name])
    log_result(account[good_folder_name]:select_all(), 'folder email post-merge')
    account:unsubscribe_mailbox(bad_folder_name)
    account:delete_mailbox(bad_folder_name)
end

function check_folder_naming(account)
    -- should be equivalent
    filtered = account['Archives/2017']:select_all()
    log_result(filtered, 'emails found')
    filtered = account['Archives.2017']:select_all()
    log_result(filtered, 'emails found')
    filtered = account['Archives/2016']:select_all()
    log_result(filtered, 'emails found')
    filtered = account['Archives.2016']:select_all()
    log_result(filtered, 'emails found')
end


-- check_folder_naming(account)
merge_folder(account)



