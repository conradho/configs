
package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"
require("common")

local florence_account = get_florence_account()


function main()
    mailboxes, folders = florence_account:list_all()
    for i, mb in ipairs(mailboxes) do
        print(mb)
    end
    for i, folder in ipairs(folders) do
        print(folder)
    end

    florence_account.Spam:check_status()
    old_spam_emails = florence_account.Spam:is_older(2)
    log_result(old_spam_emails, 'Old spam emails')
    old_spam_emails:delete_messages()
end

main()



