
package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"
require("common")

local account = get_account()
local support_account = get_support_account()


function filter_on_pythonanywhere_sender(mails)
    -- this needs to be a small subset of messages
    -- because match_from fetches those headers and does the searching locally
    -- thus can be super slow
    filtered = mails:match_from('(conrad|giles|glenn|fjl)@pythonanywhere.com')
    return filtered
end

function delete_engagement_emails(mails, email_subject)
    filtered = mails:contain_subject(email_subject) *
               (mails:contain_to('support@pythonanywhere.com') +
                mails:contain_to('developers@pythonanywhere.com'))
    log_result(filtered, 'Engagement email titled ' .. email_subject)
    filtered = filter_on_pythonanywhere_sender(filtered)
    log_result(filtered, 'Emails after regex filter on sender')
    filtered:delete_messages()
end

function find_and_delete_engagement_emails(mails)
    engagement_emails = {
        'How about', 'Schedule a task to run automatically',
        'Share the Python love', 'Check out the mighty Bash console',
        'Try out notebooks',
    }
    for _, title in ipairs(engagement_emails) do
        delete_engagement_emails(mails, '[PythonAnywhere] ' .. title)
    end
end

function delete_thanks_for_upgrading(mails)
    filtered = mails:contain_subject('Thanks for upgrading') *
               mails:contain_cc('support@pythonanywhere.com')
    log_result(filtered, 'Thanks for upgrading emails')
    filtered = filter_on_pythonanywhere_sender(filtered)
    log_result(filtered, 'Thanks for upgrading emails after regex filter on sender')
    filtered:delete_messages()
end

function delete_not_to_me(mails)
    filtered = mails - mails:contain_to('conrad@pythonanywhere.com')
                     - mails:contain_cc('conrad@pythonanywhere.com')
                     - mails:contain_bcc('conrad@pythonanywhere.com')
    log_result(filtered, 'Not directly to me')
    filtered:delete_messages()
end

function delete_over_ram_limit(mails)
    filtered = mails:contain_subject('[PythonAnywhere] Your processes have been killed') *
               mails:contain_to('support@pythonanywhere.com') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'Over ram limit')
    filtered:delete_messages()
end

function delete_dev_errors(mails)
    filtered = mails:contain_subject('conrad-dev') *
               mails:contain_to('conrad@pythonanywhere.com') *
               mails:contain_from('root@pythonanywhere')
    log_result(filtered, 'dev errors 1')
    filtered:delete_messages()
    filtered = mails:contain_subject('conrad-dev') *
               mails:contain_to('conrad@pythonanywhere.com') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'dev errors 2')
    filtered:delete_messages()
end
function delete_live_crons(mails)
    filtered = mails:contain_subject('Cron <root@') *
               (mails:contain_from('root@pythonanywhere') + mails:contain_from('support@pythonanywhere.com'))*
               mails:contain_to('batch@pythonanywhere.com')
    log_result(filtered, 'live cron emails')
    filtered:delete_messages()
end
function delete_pingdom_alerts(mails)
    filtered = mails:contain_from('alert@pingdom.com')
    log_result(filtered, 'pingdom alerts')
    filtered:delete_messages()
end


function delete_expiry_emails(mails)
    filtered = mails:contain_subject('[PythonAnywhere] Your scheduled task is about to expire') *
               mails:contain_to('support@pythonanywhere.com') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'Expiry emails')
    filtered:delete_messages()
    filtered = mails:contain_subject('[PythonAnywhere] Your website is about to expire') *
               mails:contain_to('support@pythonanywhere.com') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'Expiry emails')
    filtered:delete_messages()
end

function delete_amazon_email_failures(mails)
    filtered = mails:contain_subject('Delivery Status Notification') *
               (mails:contain_to('conrad@pythonanywhere.com') + mails:contain_to('root@pythonanywhere.com'))*
               mails:contain_from('MAILER-DAEMON@amazonses.com')
    log_result(filtered, 'Amazon deliver failure emails')
    filtered:delete_messages()
end

function delete_opsgenie_alerts(mails)
    filtered = mails:contain_from('opsgenie@opsgenie.net') *
               mails:contain_to('conrad@pythonanywhere.com')
    log_result(filtered, 'OpsGenie alerts')
    filtered:delete_messages()
end

function delete_daily_notifications(mails)
    filtered = mails:contain_subject('Daily Agenda') *
               mails:contain_to('developers@pythonanywhere.com') *
               mails:contain_from('calendar-notification@google.com')
    log_result(filtered, 'Google calendar notification')
    filtered:delete_messages()
    filtered = mails:contain_subject('[PythonAnywhere] Delinquent user report for') *
               mails:contain_to('support@pythonanywhere.com') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'Delinquent user report')
    filtered:delete_messages()
end

function delete_server_errors(mails)
    filtered = mails:contain_subject('conrad-dev')
    log_result(filtered, 'Dev env emails')
    filtered:delete_messages()
end


function main()
    -- Make sure the account is configured properly
    account.INBOX:check_status()

    -- new_mails = account.INBOX:is_unseen()

    -- selecting all might cause imapfilter to barf
    -- new_mails = account.INBOX:select_all()


    -- old_mails = account.INBOX:is_older(30)
    -- log_result(old_mails, 'old emails')
    -- old_mails:delete_messages()
    new_mails = account.INBOX:is_newer(30)


    delete_live_crons(new_mails)
    delete_pingdom_alerts(new_mails)
    delete_expiry_emails(new_mails)
    find_and_delete_engagement_emails(new_mails)
    delete_amazon_email_failures(new_mails)
    delete_opsgenie_alerts(new_mails)
    delete_thanks_for_upgrading(new_mails)

    delete_not_to_me(new_mails)

    --- may not want to run these
    --- delete_server_errors(new_mails)
    delete_dev_errors(new_mails)
    delete_over_ram_limit(new_mails)
    delete_daily_notifications(new_mails)
    account['live server errors']:select_all():delete_messages()
    account['Errors']:select_all():delete_messages()
    account['Forums']:select_all():delete_messages()
end

function support_main()
    support_account.INBOX:check_status()
    mails = support_account.INBOX:select_all()
    filtered = mails:contain_subject('ERROR')
    log_result(filtered, 'server errors')
    filtered:delete_messages()
end

main()

--support_main()

