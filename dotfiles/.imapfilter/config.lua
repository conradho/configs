-- see https://github.com/wichtounet/dotfiles/blob/master/.imapfilter/config.lua

package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"
require("common")

local account = get_account()

function move_to_folders(mails)
    print(os.date(), 'about to run move_to_folders')
    -- sent by myself (mark unread in the beginning. move to folder at the end of function)
    filtered = mails:contain_from('conrad@pythonanywhere.com') *
               mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'Own emails sent to support')
    filtered:mark_seen()

    filtered = mails:contain_from('conrad@pythonanywhere.com') *
               mails:contain_cc('support@pythonanywhere.com')
    log_result(filtered, 'Own emails cc-ing support')
    filtered:mark_seen()

    -- education
    filtered = mails:contain_subject('You have a new student') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'New student emails moved to education folder')
    filtered:mark_seen()  -- mark seen before moving them
    filtered:move_messages(account['Education'])

    filtered = mails:contain_subject('[PythonAnywhere] Student left:') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'Student left emails moved to education folder')
    filtered:mark_seen()  -- mark seen before moving them
    filtered:move_messages(account['Education'])


    -- feedback
    filtered = (
        mails:contain_subject('[PythonAnywhere]') *
        mails:contain_subject('feedback')
    )
    log_result(filtered, 'User feedback')
    filtered:move_messages(account['Feedback'])

    -- forums
    filtered = (
        mails:contain_subject('[PythonAnywhere] Forums:')
    )
    log_result(filtered, 'Forums')
    filtered:move_messages(account['Forums'])


    -- server errors
    filtered = mails:contain_subject('Internal Server Error') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('webmaster@pythonanywhere.com')
    log_result(filtered, 'Internal server errors')
    filtered:move_messages(account['live server errors'])

    filtered = mails:contain_subject('[Django] Error applying quota to user') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('webmaster@pythonanywhere.com')
    log_result(filtered, 'Quota server errors')
    filtered:move_messages(account['live server errors'])

    filtered = mails:contain_subject('Delivery Status Notification (Failure)') *
               mails:contain_from('MAILER-DAEMON@amazonses.com') *
               mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'Email delivery failures')
    filtered:move_messages(account['Errors'])


    -- news
    filtered = mails:contain_subject('Google Alert') *
               mails:contain_from('googlealerts-noreply@google.com') *
               mails:contain_to('developers@pythonanywhere.com')
    log_result(filtered, 'Google Alerts')
    filtered:move_messages(account['News'])

    -- account stuff
    filtered = mails:contain_subject('downgraded') *
               mails:contain_subject('[Django]') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('webmaster@pythonanywhere.com')
    log_result(filtered, 'Downgrades')
    filtered:move_messages(account['Accounts'])
    filtered = mails:contain_subject('upgraded') *
               mails:contain_subject('[Django]') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('webmaster@pythonanywhere.com')
    log_result(filtered, 'Upgrades')
    filtered:mark_seen()
    filtered:move_messages(account['Accounts'])

    filtered = mails:contain_subject('[PythonAnywhere] Sorry to see you go :(') *
               mails:contain_to('webmaster@pythonanywhere.com')
    log_result(filtered, 'Sorry to see you go')
    filtered:mark_seen()
    filtered:move_messages(account['Archives/2017'])

    filtered = mails:contain_subject('[PythonAnywhere] Your processes have been killed') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('webmaster@pythonanywhere.com')
    log_result(filtered, 'killed processes')
    filtered:mark_seen()
    filtered:move_messages(account['Archives/2017'])


    -- payment stuff (stripe)
    filtered = mails:contain_subject('Payment of') *
               mails:contain_from('support@stripe.com') *
               mails:contain_to('stripe@pythonanywhere.com')
    log_result(filtered, 'stripe payments')
    filtered:mark_seen()
    filtered:move_messages(account['Stripe'])

    filtered = mails:contain_subject('[PythonAnywhere] Payment failed for account') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'Payment fails (including action required)')
    filtered =  filtered - filtered:match_subject('Action required')
    log_result(filtered, 'Payment fails (without action required)')
    filtered:mark_seen()
    filtered:move_messages(account['Stripe'])

    filtered = mails:contain_subject('[PythonAnywhere] Action required: Payment failed') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'Action Required payment fails (both Paypal and Stripe)')
    -- use match_body instead of contain body for much faster search over the small subset (vs it does downloads message locally)
    filtered = filtered:match_body('automatically try to take payment again in 5 days')
    log_result(filtered, 'Action Required payment fails (just Stripe)')
    filtered:mark_seen()
    filtered:move_messages(account['Stripe'])

    -- payment stuff (paypal)
    filtered = (
        mails:contain_subject('Payment Skipped') + 
        mails:contain_subject('You received a payment') +
        mails:contain_subject('You have a new automatic payment profile for')
    ) * mails:contain_from('service@paypal.co.uk') *
        mails:contain_to('paypal@pythonanywhere.com')
    log_result(filtered, 'paypal payments')
    filtered:mark_seen()
    filtered:move_messages(account['Paypal'])

    filtered = mails:contain_subject('An automatic payment from') *
               mails:contain_from('service@paypal.co.uk')
               mails:contain_to('paypal@pythonanywhere.com')
    log_result(filtered, 'paypal automatic payment failed first glance')
    -- lua won't match any .'s with unicode in it. eh.
    -- also this needs to be a small number. match downloads and does regex search locally
    double_check_regex = filtered:match_subject('^An automatic payment from .+ failed$')
    log_result(double_check_regex, 'paypal automatic payment failed regex matched')
    double_check_regex:mark_seen()
    double_check_regex:move_messages(account['Paypal'])

    filtered = mails:contain_subject('Profile Cancelled') *
               mails:contain_from('service@paypal.co.uk')
               mails:contain_to('paypal@pythonanywhere.com')
    log_result(filtered, 'paypal payment cancelled by us')
    filtered:mark_seen()
    filtered:move_messages(account['Paypal'])

    filtered = mails:contain_subject('[PythonAnywhere] problem collecting payment for account') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'Problem collecting payment (including action required)')
    filtered =  filtered - filtered:match_subject('Action required')
    log_result(filtered, 'Problem collecting payment (without action required)')
    filtered:mark_seen()
    filtered:move_messages(account['Paypal'])


    filtered = mails:contain_subject('[PythonAnywhere] Billing Reminder') *
               mails:contain_from('support@pythonanywhere.com') *
               mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'invoice billing reminders')
    filtered:mark_seen()
    filtered:move_messages(account['Invoices'])

    -- sent by myself (put at the end, because other filters may want to move email to other folders)
    remaining_new_mails = account.INBOX:is_newer(10)
    filtered = remaining_new_mails:contain_from('conrad@pythonanywhere.com') *
               remaining_new_mails:contain_to('support@pythonanywhere.com')
    log_result(filtered, 'Own emails sent to support')
    filtered:move_messages(account['Sent by Me'])

    filtered = remaining_new_mails:contain_from('conrad@pythonanywhere.com') *
               remaining_new_mails:contain_cc('support@pythonanywhere.com')
    log_result(filtered, 'Own emails cc-ing support')
    filtered:move_messages(account['Sent by Me'])

end

function delete_spammy(mails)
    print(os.date(), 'about to run delete_spammy')
    -- loud cron processes
    filtered = mails:contain_subject('Cron') *
               mails:contain_subject('hung_quota_check') *
               mails:contain_from('root@pythonanywhere')
    log_result(filtered, 'hung_quota_check')
    filtered:delete_messages()
    filtered = mails:contain_subject('Cron') *
               mails:contain_subject('process_killer.py') *
               mails:contain_from('root@pythonanywhere')
    log_result(filtered, 'process_killer')
    filtered:delete_messages()

    -- email rsync delete errors
    filtered = mails:contain_subject('Cron <root@darcachon> rsync') *
               mails:contain_from('root@resolversystems.com')
    log_result(filtered, 'rsync')
    filtered:delete_messages()


end

function delete_kubernetes_errors(mails)
    filtered = mails:contain_subject('[Django] Error talking to kubernetes API doing a POST to /') *
               mails:contain_to('webmaster@pythonanywhere.com') *
               mails:contain_from('support@pythonanywhere.com')
    log_result(filtered, 'kubernetes errors')
    filtered:delete_messages()
end

function delete_trash(account)
    print(os.date(), 'about to run delete_trash')
    filtered = account.Trash:select_all()
    log_result(filtered, 'Trash')
    filtered:delete_messages()
end

function main()
    -- Make sure the account is configured properly
    account.INBOX:check_status()
    
    --[[
    mailboxes, folders = account:list_all()
    for k, v in pairs(folders) do print(k, v) end
    for k, v in pairs(mailboxes) do
        print(k, v)
    end
    --]]

    -- new_mails = account.INBOX:is_unseen()
    -- new_mails = account.INBOX:select_all()
    new_mails = account.INBOX:is_newer(30)
    move_to_folders(new_mails)
    delete_spammy(new_mails)
    delete_kubernetes_errors(new_mails)
    delete_trash(account)
    
end

main()



