package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"

require("common")
local account = get_account()



function delete_folders(account)
    for i, name in ipairs(bad_folder_names) do
        account:delete_mailbox(name)
    end
end

function log_if_folders_still_exist(account)
    for i, name in ipairs(bad_folder_names) do
        log_result(account[name]:select_all(), 'emails found')
    end
end


bad_folder_names = {'postpone', 'postponed', 'Drafts'}
delete_folders(account)
log_if_folders_still_exist(account)



