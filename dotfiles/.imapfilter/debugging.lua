-- see https://github.com/wichtounet/dotfiles/blob/master/.imapfilter/config.lua

package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"
require("common")

local account = get_support_account()

function main()
    -- Make sure the account is configured properly
    account.INBOX:check_status()
    account.Templates:check_status()

    mailboxes, folders = account:list_all()
    for k, v in pairs(folders) do print(k, v) end
    for k, v in pairs(mailboxes) do
        print(k, v)
    end

    for _, msg in ipairs(account.Templates:select_all()) do
        mbox, uid = unpack(msg)
        print(mbox[uid]:fetch_field('subject'))
    end

end

main()



