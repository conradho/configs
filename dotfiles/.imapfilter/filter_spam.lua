package.path = package.path .. ";" .. os.getenv("HOME") .. "/.imapfilter/?.lua"
require("common")

local account = get_account()

function main()
    -- just a quick check to see account is correct
    account.INBOX:check_status()

    to_be_deleted = Set {}

    for _, message in ipairs(account.INBOX:is_unseen()) do
        mbox, uid = table.unpack(message)
        text = mbox[uid]:fetch_message()
        if (pipe_to('my_spam_filter.py', text) == 1) then
            table.insert(to_be_deleted, message)
        end
    end

    log_result(to_be_deleted, 'Found this much spam')
    to_be_deleted:delete_messages()
end

main()



