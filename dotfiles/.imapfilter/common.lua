
io.stdout:setvbuf("line")

options.limit = 100
options.range = 100

function get_account()
    return IMAP {
        server = os.getenv('REMOTE_SERVER'),
        username = os.getenv('USER'),
        password = os.getenv('PASSWORD'),
        ssl = 'tls13',
    }
end
function get_support_account()
    return IMAP {
        server = os.getenv('REMOTE_SERVER'),
        username = os.getenv('SUPPORT_USER'),
        password = os.getenv('SUPPORT_PASSWORD'),
        ssl = 'tls13',
    }
end
function get_florence_account()
    return IMAP {
        server = os.getenv('REMOTE_SERVER'),
        username = os.getenv('FLORENCE_USER'),
        password = os.getenv('FLORENCE_PASSWORD'),
        ssl = 'tls13',
    }
end

function log_result(filtered, message)
    count = 0
    for k, v in ipairs(filtered) do
        count = count + 1
    end
    if count ~= 0 then
        print(os.date(), message, count)
    end
end

