# source bashrc in bash console to check for errors whenever you edit this file

# if not running interactively, stop loading configs
case $- in
    *i*) ;; # running interactively
      *) return;;
esac

# change bash readline to vim settings
set -o vi
# functions and alias definitions:
# put any local ones in bash_aliases
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi
# common aliases shared with fish/other shells
if [ -f ~/.shell_aliases ]; then
    source ~/.shell_aliases
fi

export PATH=~/.local/bin:/usr/local/bin:/usr/bin:$PATH
# ugly solution, but lets do this for now
export PATH=$PATH:~/.gem/ruby/1.9.1/bin
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin

# makes it so that cli tab completion and vim ignores pyc files
export FIGNORE=$FIGNORE:.pyc


export VIRTUALENVWRAPPER_PYTHON=`which python3`
if [ "$(uname)" == "Darwin" ]; then
    # mac brew install linked binaries needs /usr/local/bin ahead of /usr/bin
    # but this should already be true
    # export PATH=$(brew --prefix)/bin:$PATH

    # mac needs reattach-to-user-namespace for vim copy/paste to work
    alias tmux='SHELL=`which fish` COMMAND="reattach-to-user-namespace -l fish" tmux'
    alias ll='ls -ABhl | less -R'
    # make git branches autocomplete etc, need to brew install bash-completion for mac
    source $(brew --prefix)/etc/bash_completion
    source $(brew --prefix)/etc/profile.d/z.sh
    source virtualenvwrapper.sh
else
    alias tmux='SHELL=`which fish` tmux'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias ls='ls --color=auto'
    # "ls | less" won't have color
    alias ll='ls -ABhl --color=always | less -R'
    # Add an "alert" alias for long running commands. Will create a popup. Use like so:
    #   sleep 10; alert
    alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
    source /usr/share/bash-completion/bash_completion
    source virtualenvwrapper.sh
fi

# "goto admin user_abc"
function goto () {
    # if username (or email for the admin search) was passed in
    if [ "$2" ]; then
        echo $2 | xclip -i -sel primary;
        echo $2 | xclip -i -sel clipboard;
        # just set it into both clipboards to avoid confusion
        # technically only need primary one for script to run
    fi
    case "$1" in
        admin)
            ~/configs/utils_bin/goto_pythonanywhere.py admin;;
        console)
            ~/configs/utils_bin/goto_pythonanywhere.py console;;
        forum)
            ~/configs/utils_bin/goto_pythonanywhere.py forum;;
        *)
            echo 'hmm';;
    esac
}

# must use single quotes because we need inner shellout to be evaluated dynamically
alias add='pushd $(git rev-parse --show-toplevel) && git diff --ignore-blank-lines --no-color | git apply --cached - && popd'

function v () {
    # to debug, try to run with -D and -V
    vim "$1"
}
function g () {
    cd "$1";
    ls -a;
}

# searches processes in ps aux and kills matching processes
function killx () {
    #    echo $1;
    #    echo "\$1";
    kill `ps aux | grep -i $1 | grep -v grep | awk '{print $2}'`;
}

# funny harry function
# "fuck you firefox"
# can have creative use of second word
# need to have flip program in path
function fuck() {
    killall -9 $2;
    if [ $? == 0 ]
    then
        {
            echo
            echo "              ︵"
            echo "             ҂ $(echo $2|flip &2>/dev/null) ҂"
            printf "    (╯°□°)╯   %${#2}s  ͝            ~<:>>>>>>>>>"
            echo
        } | lolcat
        # need to gem install lolcat
    fi
}

# reference http://rabexc.org/posts/pitfalls-of-ssh-agents
ssh-agent-is-running() {
    [ -S "$SSH_AUTH_SOCK" ] && { ssh-add -l >& /dev/null || [ $? -ne 2 ]; } 
}
ssh-agent-has-no-keys() {
    ssh-add -l | grep "no identities" >& /dev/null
}
export SSH_AGENT_ENV_VAR_SCRIPT="/tmp/.$(whoami)-$(hostname)-ssh-agent-script.sh"

if ! ssh-agent-is-running; then
    echo had to start ssh-agent from scratch
    (umask 066; ssh-agent > $SSH_AGENT_ENV_VAR_SCRIPT)
fi
if [ ! -f $SSH_AGENT_ENV_VAR_SCRIPT ]; then
    # an ssh-agent could be running without having populated the env variables script if somehow the system starts one for you
    echo "ssh-agent running but can't find environment variables file, starting new ssh-agent and repopulating environment variables script"
    (umask 066; ssh-agent > $SSH_AGENT_ENV_VAR_SCRIPT)
fi
# load the ssh-agent environment variables
eval "$(<$SSH_AGENT_ENV_VAR_SCRIPT)" >/dev/null

if ssh-agent-has-no-keys; then
    # keys expire in 1 day
    ssh-add -t 86400
    # will just skip if cannot find any of ~/.ssh/id_rsa, id_dsa, id_ecdsa, id_ed25519, identity
fi



# makes normal terminal (and first terminal running tmux also 256 color)
if [ -n "$DISPLAY" -a "$TERM" == "xterm" ]; then
    export TERM=xterm-256color
    # mac setting?
    # export TERM='screen-256color-bce'
fi 

# customize command prompt
# Show current git branch (for use in prompt later)
function parse_git_branch {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[\1]/'
}
function virtualenv_info(){
    # default to empty string if VIRTUAL_ENV is unset, so we don't run into any
    # errors when the nounset bash option is enabled
    INFO=${VIRTUAL_ENV:-}
    # If we have a virtualenv, strip out the path and just leave the env name
    if [[ ! -z $INFO ]]; then
        INFO="${VIRTUAL_ENV##*/}"
    fi
    [[ -n "$INFO" ]] && echo "[venv:$INFO]"
}

# disable the default virtualenv prompt change
export VIRTUAL_ENV_DISABLE_PROMPT=1


# note: \e[ and \e[m are the start and end delimiters for color scheme (could also be \033 instead of \e)
# 0;30m etc are the color to use and whether it is bolded
# note: put \[ and \] around color sequences
BLACK="\[\033[0;30m\]"
RED="\[\033[0;31m\]"
GREEN="\[\033[0;32m\]"
LIGHT_GREEN="\[\033[1;32m\]"  # also bolds it...
YELLOW="\[\033[0;33m\]"
BLUE="\[\033[0;34m\]"
PURPLE="\[\033[0;35m\]"
CYAN="\[\033[0;36m\]"  #torquise-ish
LIGHT_GREY="\[\033[0;37m\]" # more or less white
NO_COLOR="\[\033[0m\]"


# \u username \h hostname \W cwd \$ displays $ (if root then displays #)
export PS1="$BLUE\u@\h:$PURPLE\W"
export PS1="$RED${debian_chroot:+($debian_chroot)}$PS1"
export PS1="\n$PS1$CYAN\$(virtualenv_info)$BLUE\$(parse_git_branch)\$ $GREEN"
# notice that the last color above changes the commandline text entry color

###########################################################################
# if there is a bullshit brackets that pops up after you hit enter
# and is bracketing your prompt on the left and the edge of the screen on the right
# it is mac being a dick. uncheck `view -> show marks`
###########################################################################

# below turns the console output back into normal color vs prompt and the command text
# note it doesn't have the \] closing like colors above
# will have to set what color this text (eg; to white) in terminal settings
if [ "$(uname)" == "Darwin" ]; then
    trap 'echo -ne "\033[0m";' DEBUG
else
    trap 'echo -ne "\e[0m";' DEBUG
fi
# customize colors for ls
export CLICOLOR=exfxcxdxbxegedabagacad
 # this is needed for mac
export LSCOLORS=exfxcxdxbxegedabagacad
# colors
# a black b red c green d brown e blue f magenta g cyan h light grey
# A bold black, usually shows up as dark grey B bold red C bold green
# D bold brown, usually shows up as yellow E bold blue F bold magenta G bold cyan
# H bold light grey; looks like bright white x default foreground or background

# The order of the attributes are as follows:
#  1.   directory
#  2.   symbolic link
#  3.   socket
#  4.   pipe
#  5.   executable
#  6.   block special
#  7.   character special
#  8.   executable with setuid bit set
#  9.   executable with setgid bit set
#  10.  directory writable to others, with sticky bit
#  11.  directory writable to others, without sticky bit


# otherwise mac gets weird errors with pylint etc (unknown locale: utf-8)
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# put the following line into /etc/inputrc
# to make bash case insensitive
# set completion-ignore-case on


export NVM_DIR=~/.nvm
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
# load the newest node. if there is newer version, use nvm install to get it

# ansible already sets `nvm alias default v5.1.1` which should make your default nvm v5.1.1
# note: default and current etc are special keywords in nvm
# nvm use default


# don't put lines starting with space in history
# erase duplicated commands (but bring it fwd)
HISTCONTROL=ignorespace:erasedups
# append to history file, don't overwrite
shopt -s histappend
# set history length and file size
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set default editor
export EDITOR=vim

