Some useful commands:

    # to get all *enabled* timers for the user space
    systemctl list-timers --all --user

    # after any changes to the files
    systemctl --user daemon-reload

    # to start a service manually instead of waiting for a timer to trigger it
    systemctl --user start test-quick-fail

    # alternatively if your timer didn't startup on boot, could do the same to start the timer
    systemctl --user start offlineimap-oneshot.timer

    # to see the log
    journalctl --user  -u offlineimap-oneshot.service

The user services vs the system wide services don't mix.

You need to systemctl enable the timer but not the service; doing the enable
creates the timers.target.wants and other relevant folders, which is already
checked-in to this repo

If you could also put services on the system-wide system:
    /etc/systemd/system/email-sender.service
    /etc/systemd/system/email-sender.timer

or another place for a user service (instead of ~/.config/systemd/user) is:
    /etc/systemd/user/offlineimap-oneshot.service
    /etc/systemd/user/offlineimap-oneshot.timer

Then you can access the timer or the service with the --user flag
    systemctl --user status offlineimap-oneshot.service
    journalctl --user  -u offlineimap-oneshot.service

If you had saved it in /etc/systemd/system/, then you need to access as
    sudo systemctl status email-sender.timer
    sudo journalctl -u email-sender.timer

