# fish git prompt
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showupstream 'yes'
set __fish_git_prompt_color_branch yellow

# Status Chars
set __fish_git_prompt_char_dirtystate '⚡'
set __fish_git_prompt_char_stagedstate '→'
set __fish_git_prompt_char_stashstate '↩'
set __fish_git_prompt_char_upstream_ahead '↑'
set __fish_git_prompt_char_upstream_behind '↓'
 
function fish_prompt
    # save last status before we do anything here (which would overwrite it)
    set last_status $status
    if test $status != 0
        set_color red
        echo "last command failed with return code $last_status"
    end
    set_color blue
    printf (date "+$c2%H$c0:$c2%M$c0:$c2%S")
    printf ' %s@%s ' $USER (hostname)
    if set -q VIRTUAL_ENV
        # set_color -b blue white
        echo -n -s (set_color red) "[" (basename "$VIRTUAL_ENV") "]" (set_color normal)
    end
    set_color normal
    printf '%s\n' (__fish_git_prompt)
    set_color $fish_color_cwd
    printf '%s $ ' (prompt_pwd)
    set_color normal
end

