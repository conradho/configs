function add
    pushd (git rev-parse --show-toplevel)
    git diff --ignore-blank-lines --no-color | git apply --cached -
    popd
end
