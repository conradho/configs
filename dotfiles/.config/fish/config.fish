# make virtualenv commands the same on bash vs fish
alias deactivate="vf deactivate"
alias workon="vf activate"

source ~/.shell_aliases

fish_add_path -gp ~/.local/bin
