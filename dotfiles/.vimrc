" vim (not vi), must be first. changes behavior of other settings
set nocompatible
set noesckeys


" sync vim defaulg register with clipboard register?
" make sure you have apt-get install vim-gnome
" double check you have +clipboard support with 'vim --verion | grep clipboard'
set clipboard=unnamed

" techincally, should wrap all autocmd stuff in a 
" if has('autocmd') block
" but too lazy to do so (easier to group lines into relavant sections)
" however, need to make augroup so autocmds dont get double executed
" then au! inside the group clears autocmds before they get executed again

" 256 colors
set t_Co=256
 
" if shell is fish, then everyone complains about redirection
set shell=/bin/bash

" set fileformats for compatibility
" whichever fileformat comes first will be the default
" unless you set fileformat=x later
set fileformats=unix,dos
" behave mswin
augroup readonly

    " Note that `vim`, vs `:edit file`, vs `vim demo.txt` have 3 diff event orders
	">>> start vim
    "        	BufWinEnter     (create a default window)
    "        	BufEnter        (create a default buffer)
    "        	VimEnter        (start the Vim session)
    "        	BufRead         (starting to edit a new buffer, after reading the file)
    " :edit demo.txt
    "        	BufNew          (create a new buffer to contain demo.txt)
    "        	BufAdd          (add that new buffer to the session’s buffer list)
    "        	BufLeave        (exit the default buffer)
    "        	BufWinLeave     (exit the default window)
    "        	BufUnload       (remove the default buffer from the buffer list)
    "        	BufDelete       (deallocate the default buffer)
    "        	BufReadCmd      (read the contexts of demo.txt into the new buffer)
    "        	BufRead         (starting to edit a new buffer, after reading the file)
    "        	BufEnter        (activate the new buffer)
    "        	BufWinEnter     (activate the new buffer's window)
    " i
    "        	InsertEnter     (swap into Insert mode)
    " hi<ESC>
    "           CursorMovedI    (insert a character)
    "           CursorMovedI    (insert a character)
    "           CursorMovedI    (insert a character)<ESC>
    "           InsertLeave     (swap back to Normal mode):wq
    " :wq
    "           BufWriteCmd     (save the buffer contents back to disk)
    "           BufWinLeave     (exit the buffer's window)
    "           BufUnload       (remove the buffer from the buffer list)
    "           VimLeavePre     (get ready to quit Vim)
    "           VimLeave        (quit Vim)
    " Now here is the other case
	">>> vim demo.txt
    "        	BufRead         (starting to edit a new buffer, after reading the file)
    "        	BufWinEnter     (create a default window)
    "        	BufEnter        (create a default buffer)
    "        	VimEnter        (start the Vim session)

    " To debug:
    " 1. you probably need to remove ~/.vim/view
    "    "(which are saved file settings like folds, readonly etc)
    " 2. from open editor `:echo &l:modifiable` or `:echo &g:modifiable`
    " 3. from within the au `echom "BufWinEnter"`, then from the open editor `:messages`

    " only set unix as default file format if file is writeable
	" should perhaps check (if &ft!='qf') (skip quickfix windows)
	autocmd BufRead,BufWinEnter,BufEnter * if &modifiable | set fileformat=unix | endif
        " we could also make sure any not modifiable stuff is also not readonly etc
		"\|  set nomodifiable
		"\|  set readonly
    " how to trigger other autocmds
    " autocmd VimEnter * doauto BufRead
augroup END

set encoding=utf-8
"put swp files and ~ files into /.vim/temp
"to figure out windows location without spaces, 
"go to \C. cmd dir \x
"then see that 'program files (x86)' has 
"shortform 'PROGRA~2'
" set backupdir=~/Temp
" set directory=~/Temp
set nobackup
set noswapfile

" if diff is on, then can't use syntastic linter etc
diffoff

" syntax highlighting
syntax on
"" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo'


augroup vim_basics_group
    autocmd!
    " make sure highlighting works all the way down long files
    autocmd BufEnter * :syntax sync fromstart

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif
    
    " save the za folds for access next time- this conflicts with the readonly augroup
    " if you want to enable this, put them inside of the readonly augroup and
    " prob loadview first and then rewrite modifiable and readonly variables after
    " autocmd BufWritePost * silent! mkview
    " autocmd BufWinEnter * silent! loadview

    " update and implement vimrc chgs once it is written
    " might have some problems with recursion/duplication
    autocmd bufwritepost $MYVIMRC source $MYVIMRC

    "auto open error window on make/compiling
    autocmd QuickFixCmdPost [^l]* nested copen
    autocmd QuickFixCmdPost    l* nested lopen

augroup END

" might just be a mac thing
" allow cursor to be positioned one char past end of line
" and apply operations to all of selection including last char
" set selection=exclusive

" allow chging buffers without saving
set hidden

" allow backspacing over everything (new line etc)
set backspace=indent,eol,start
" when joining lines, don't insert two spaces after a punctuation
set nojoinspaces
set expandtab

" search related stuff
" make searches case-sensitive only if they contain upper case characters
set ignorecase
set smartcase
" search highlighting 
" and highlight next match as you are typing search
set hlsearch incsearch
" exit search highlight mode by pressing enter again
" go to next error item (:lnext) if the error window is opened
nnoremap <CR> :set hlsearch!<CR><CR>
nnoremap <TAB> :lnext<CR>
" search backwards if <S+TAB> is pressed
" just try Ctrl+Shift+v then Shift + Tab to get the below <S+Tab> character
set <S-TAB>=[Z
" nnoremap [1;2Z :lprev<CR>
nnoremap <S-TAB> :lprev<CR>
nnoremap <F3> :cnext<CR>
" nnoremap <S-F3> :cprev<CR>

" do nothing if shift tab ([1;2Z) is detected in insert mode
inoremap [1;2Z <Nop>
" cnoremap [1;2Z <Nop>

" search-next wraps back to start of file
set wrapscan


" tab completion dropbown menu for files/buffers
" to customize more, chg wildmode
set wildmenu

" check if running on mac
let s:uname = system("echo -n \"$(uname)\"")

" tab complete of file names will ignore case
" if this is not recognized, update your vim
try
    if s:uname != "Darwin"
        set fileignorecase
    endif
catch
    " add silent so that echo won't cause [press enter to continue]
    silent !echo "fileignorecase keyword was not recognized. maybe old version of vim?"
endtry

" make the leader commands/motion commands#
" show up in the command bar
" will also display number of selected lines etc
set showcmd
" show matching brackets
set showmatch
" bracket disappears after x/10th of a second
" set matchtime=1

" enable automatic yanking to and pasting from selection
" set clipboard+=unnamed


" always enable status line
set laststatus=2
" setup statusline format
set statusline=%t\    "vs %f is name relative to cwd"
set statusline+=\  " (use \ as separator)

" file type
" set statusline+=%{strlen(&ft)?&ft:'none'}
"file type- just show nothing if none
set statusline+=%y
"file format (unix/dos)"
" set statusline+=%{&ff}\

" stuff that only show up if they exist
" modified flag
set statusline+=%#wildmenu#
set statusline+=%M  " %m gives [+] instead of +
set statusline+=%*

"Display a warning if file encoding isnt utf-8
set statusline+=%#question#
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
set statusline+=%*

"display a warning if fileformat isnt unix
set statusline+=%#directory#
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*

"display a warning if files contains tab chars
set statusline+=%#warningmsg#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%*

" read-only
set statusline+=%r
set statusline+=%*

"go to right hand side of status bar"
set statusline+=%=
"%{} evaluates everything inside of it
" returns gets the file's location, using ~ shortcuts"
" wraps the location in [] and cuts it at 50 characters
set statusline+=[%<%-.50{expand('%:~:h')}\\]
"column (counting tab/whitespace) otherwise %c
set statusline+=\ %v\| 
"row number: %l
set statusline+=%P\ of\ %L
"total rows vs total"

" the tab warning flag that shows up on statusline:
" return '[tabs]' if tab chars in file, or empty string
function! StatuslineTabWarning()
    if !exists("b:statusline_tab_warning")
        let tabs = search('^\t', 'nw') != 0

        if tabs
            let b:statusline_tab_warning = '[tabs!]'
        else
            let b:statusline_tab_warning = ''
        endif
    endif
    return b:statusline_tab_warning
endfunction
augroup recalculate_tab_warning_group
    au!
    " recalculate the tab warning flag when idle and after writing
    autocmd cursorhold,bufwritepost * unlet! b:statusline_tab_warning
augroup END

" call :DiffSaved to get a split screen diff vs last save
" otherwise :w !diff % - could also work, but shittier
function! s:DiffWithSaved()
    let filetype=&ft
    diffthis
    vnew | r # | normal! 1Gdd
    diffthis
    exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
command! DiffSaved call s:DiffWithSaved()
" call :DiffGit to get a diff vs git checkout
function! s:DiffWithGITCheckedOut()
  let filetype=&ft
  diffthis
  vnew | exe "%!git diff " . expand("#:p:h") . "| patch -p 1 -Rs -o /dev/stdout"
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
  diffthis
endfunction
com! DiffGit call s:DiffWithGITCheckedOut()


" all the actual key remappings
" this needs to be before all the references to leader below
let mapleader = " "

" nnoremap does not remap recursively and only for normal mode
" remaps <leader>hjkl to ctrl+w hjkl to move buffers quickly
" note alt keys don't work well with terminal (gets caught somewhere else)
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l
nnoremap <leader>q <C-w>q
nnoremap <leader>v <C-w>v
nnoremap <leader>s <C-w>s

set splitbelow
set splitright

" resize panes
nnoremap <leader>H <C-w>10h:vertical resize -10<return>
nnoremap <leader>J <C-w>10j:resize -5<return>
nnoremap <leader>K <C-w>10k:resize -5<return>
nnoremap <leader>L <C-w>10l:vertical resize -10<return>
" C-w = to restore to all equal size



" leader + V to replace ctrl+v because some environments steal ctrl+v focus etc
nnoremap <leader>V <C-v>j
" force screen update if text get messed up
" also clear the search highlight by setting it the @ register to ''
nnoremap <leader>r :let @/=''<CR>:redraw!<CR>
" command to edit vimrc save time from opening it
nnoremap <leader>ev :e $MYVIMRC<CR>
" toggles copy and paste mode so indenting doesn't get screwed up
nnoremap <leader>p :set paste!<CR>
" toggles wrapline
nnoremap <leader>w :set wrap!<CR>
" toggle tabs/indentation
nnoremap <leader>i :set expandtab tabstop=4 shiftwidth=4 softtabstop=4<CR>:%s/\t/    /<CR>
nnoremap <leader>I :set expandtab tabstop=2 shiftwidth=2 softtabstop=2<CR>:%s/\t/  /<CR>

" space space opens the CtrlP fuzzy finder
nnoremap <leader>f :CtrlP<CR>
nnoremap <leader>b :CtrlPBuffer<CR>
nnoremap <leader>t :CtrlPTag<CR>
let g:ctrlp_custom_ignore = 'node_modules\|static\|vendors\|DS_Store'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*/.git/*
set wildignore+=*\\tmp\\*,*.exe   "windows specific

" remap below purposely cannabalizes the rest of easy motion
" this is because the rest is mapped to <leader><leader>e
" theoretically the whole of easymotion still works in visual mode
nmap <leader><leader> <Plug>(easymotion-s2)
" lets at least let it work a little bit
map <leader>e <Plug>(easymotion-prefix)
let g:EasyMotion_smartcase=1

" toggles upper/lower case during normal mode
nnoremap <leader>u g~iw
" change word to upper case when in insert mode
inoremap <C-u> <esc>gUiwi

" for long lines, j goes to same line next row
nnoremap j gj
nnoremap k gk

" allow cursor to scroll right aff end of one line to start of next line
" set whichwrap+=<,>,[,],h,l

" if needed sudo access to write file
cmap w!! w !sudo tee % >/dev/null

" after a write, generate an errors buffer
" NB: if u comment this out, need to restart whole vim for it to take effect
cmap w<CR> w<CR>:Errors<CR>

" grep for word under cursor (<C-r><C-w> is the word under the cursor)
" recursively looks at files with word inside file
" starting from current directory
nnoremap <leader>G :grep --exclude-dir=node_modules -rw '<C-r><C-w>' .<CR>

" below is command to grep within your file and find instances of searchword
" and put into a new buffer. # means the previous buffer name
" :new | read !grep searchword #
" need \ to escape the |
nnoremap <leader>g :below new \| :10winc - \| read !grep '<C-r><C-w>' #<CR>

" for sorting and formatting python imports
" [( goes to prev unmatched (
" ]) goes to next unmatched )
" !python just takes the line under visual selection
" strip white spaces in front of words or sorting fails
" filter out empty strings
" gq formats the selection
nnoremap <leader>( [(jv])k:join<CR>v:!python -c "import sys; words = map(lambda x: x.strip(), sys.stdin.read().split(',')); print ', '.join(sorted(filter(None, words))) + ','"<CR>>>Vgq


" this works even if you had xmodmap etc already switching : ; because you
" don't need to switch : back to ; (just make everything :)
noremap ; :

augroup vimgroup
    au!

    " change status line color based on mode
    " fg = font color; bg = background BUT need to set term/cterm != reverse, and it also defaults to bold
    if version >= 700
        autocmd InsertEnter * hi StatusLine term=NONE cterm=NONE gui=None ctermbg=DarkRed ctermfg=LightGrey
        autocmd InsertLeave * hi StatusLine term=NONE cterm=NONE gui=NONE ctermbg=Black ctermfg=LightGrey
        autocmd BufEnter * hi StatusLine term=NONE cterm=NONE gui=NONE ctermbg=DarkMagenta ctermfg=LightGrey
        autocmd BufEnter * hi StatusLineNC term=NONE cterm=NONE gui=NONE ctermbg=Black ctermfg=White
    endif

augroup END

" here comes all the plugins
filetype plugin indent on

if has("autocmd")
    " pep8 etc needs this to run (which in turn needs autocmd)
    filetype plugin indent on
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
else
    " if old vim, set vanilla autoindenting on
    set autoindent
endif " has("autocmd")

"setup pathogen and plugins
call pathogen#infect()
call pathogen#helptags()

" Configure the python checker to call a Python 3 interpreter rather than Python 2
let g:syntastic_python_python_exec = 'python3'
let g:black_linelength = 117
set textwidth=117
let g:vim_isort_python_version = 'python3'
" disable any isort shortcuts, since we autorun it on save anyways
let g:vim_isort_map = ''
" use flake8, not pyflakes or pylint. if it doesn't work you need to pip install it, or pip3 install it
let g:syntastic_python_checkers=['flake8']
" node install -g eslint eslint-plugin-react
let g:syntastic_javascript_checkers=['eslint']

let g:UltiSnipsUsePythonVersion=3

" will try to fix the fixable errors BEFORE writing
" can :e to see the changes after writing, but doesn't really matter (it is already saved)
" it may not seem like the fixes have reloaded, but it has if you cat the file
let g:syntastic_javascript_eslint_args="--fix --config ~/.eslintrc.yml"
" gundo plugin (redo/undo tree)
nnoremap <F1> :GundoToggle<CR>

" nerdtree (similar to :Ex)
nnoremap <leader>n :NERDTreeToggle<CR>
let g:NERDTreeQuitOnOpen = 1  "close after opening a file with o, i, t or T

"open nerdtree on startup if not opening a specific file
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

nnoremap <F2> :TagbarToggle<CR>
let Tlist_Exit_OnlyWindow = 1 " exit if taglist is last window open 
let Tlist_Show_One_File = 1 " Only show tags for current buffer 
let Tlist_Use_Right_Window = 0 " Open on right side
" TagBar actually dynamically generates tags instead of using tags files
" but to say find tag in another file using Ctrl+]
" we want to be able to search up parentdir for tags file (by adding the ;)
set tags=tags;
let g:tagbar_sort = 0  "don't sort alphabetically by default. just by file order
let g:tagbar_type_markdown = {
  \ 'ctagstype' : 'markdown',
  \ 'kinds' : [
    \ 'h:Heading_L1',
    \ 'i:Heading_L2',
    \ 'k:Heading_L3'
  \ ],
  \ 'deffile': '~/.ctags'
\ }
" add config file location because some implementations of ctag (eg:
" universsal ctag) do not look at ~/.ctags (they expect ~/.ctags.d/js.ctags)
let g:tagbar_type_javascript = {
  \ 'ctagstype' : 'javascript',
  \ 'kinds' : [
    \ 'd:describe',
    \ 'c:class',
    \ 'a:class methods',
    \ 'i:it',
    \ 'x:xit'
  \ ],
  \ 'deffile': '~/.ctags'
\ }

" fugitive plugin (make git commands G[command])
" nnoremap <leader>gadd :Gwrite
" nnoremap <leader>grm :Gremove
" nnoremap <leader>gmv :Gmove
" nnoremap <leader>gcommit :Gcommit
" other commands include :Gread == :Git checkout % == revert 
" all changes to git save


" set <F5> to run python script, and get the results inserted into the file
" if executed in visual mode, then the text block selected will be
" evaluated as python code; 
nnoremap <F5> G:read !python "%"<CR>
" @" is the most recent register
" also interesting is that <C-r>" actually pastes in the most recent register
vnoremap <F5> y:let RunResult = system('python', @")<CR>:put =RunResult<CR>

" have a pretty score etc
nnoremap <F6> :!pylint %<CR>
" set <F7> to automatically email code
nnoremap <F7> :TOhtml<CR>:!vim_email.py -f "%:p"<CR>:w<bar>bd<CR>
" <F8> is the default for vim-colorscheme-switcher
" <F9> to go back to original colorscheme
nnoremap <F9> :colorscheme peachpuff<CR>

" remap nerdcommenter. can do stuff like 4<leader>c to comment 4 lines
nnoremap <leader>c :call NERDComment('n', 'Comment')<CR>
vnoremap <leader>c :call NERDComment('x', 'Comment')<CR>
nnoremap <leader>C :call NERDComment('n', 'Uncomment')<CR>
vnoremap <leader>C :call NERDComment('x', 'Uncomment')<CR>


set tabstop=4
set shiftwidth=4
set softtabstop=4
"indent rounded to next full shift width
set shiftround
"auto-indent code blocks
set autoindent
" show line numbers, and as relative instead of absolute
set relativenumber
" show absolute line number on the cursor line
set number
" set code folding (press za to open/close and zm/zr to fold in levels)
set foldmethod=indent
set foldlevelstart=5
" don't breakup lines longer than textwidth=x
set textwidth=0
" do not show whitespace (python au will show whitespace later)
set nolist


" make emails written in mutt use format-flow 
" http://wcm1.web.rice.edu/mutt-tips.html
" setlocal fo+=aw

augroup shell_group
    au!
    autocmd BufNewFile,BufRead .shell_aliases set ft=sh
augroup END

augroup python_group
    " TODO: make everything buffer specific
    " long line and \t becomes matched in vimrc
    
    au!

    " tab width = 4 spaces
    au FileType python setlocal tabstop=4
    " indent width = 4
    au FileType python setlocal shiftwidth=4

    "delete multiple spaces like it was a single tab
    au FileType python setlocal softtabstop=4

    " expand tabs to spaces
    au FileType python setlocal expandtab

    " autocmd FileType python set nowrap
    " show tabs as >----
    autocmd FileType python setlocal list
    autocmd FileType python setlocal listchars=tab:>-

    " shortcut to don't-ify tests, except for the test you are on
    au Filetype python noremap <leader>d jma:%s/def test/def DONTtest/<CR>`a?def DONT<CR>wdtt:w<CR>
    au Filetype python noremap <leader>D ma:%s/def DONTtest/def test/<CR>`a:w<CR>

    " not wrap lines since python lines shouldn't go past 80 chars
    " to only highlight letters > 70: use '\%>50v.\+'
    " b: means specific to this buffer"
    autocmd BufEnter *.py
        \ let bad_tab=matchadd("ErrorMsg", '\t')
        " \| let long_line=matchadd("IncSearch", '^.\{118\}.*$')
    " makes nonpython docs not match"
    autocmd BufLeave *.py
        \ call clearmatches()

    autocmd BufWritePre *.py execute ':Isort'
    " automatically run black, after isort, so isort doesn't fuck things up
    autocmd BufWritePre *.py execute ':Black'

augroup END

augroup html_group
    au!
    autocmd FileType html
        \ setlocal nowrap
        \| setlocal tabstop=2
        \| setlocal softtabstop=2
        \| setlocal shiftwidth=2
    autocmd FileType djangohtml
        \ setlocal nowrap
        \| setlocal tabstop=2
        \| setlocal softtabstop=2
        \| setlocal shiftwidth=2
    autocmd FileType htmldjango
        \ setlocal nowrap
        \| setlocal tabstop=2
        \| setlocal softtabstop=2
        \| setlocal shiftwidth=2
augroup END


augroup json_group
    au!
    autocmd BufNewFile,BufRead *.json set ft=json
augroup END

augroup js_group
    au!
    autocmd FileType javascript
        \ setlocal nowrap
        \| setlocal tabstop=2
        \| setlocal softtabstop=2
        \| setlocal shiftwidth=2
    " turn on concealing
    autocmd BufEnter *.js set conceallevel=1
    autocmd FileType typescript setlocal formatprg=prettier\ --parser\ typescript

augroup END


" let g:javascript_conceal_function   = "ƒ"
let g:javascript_conceal_function   = "λ"
let g:javascript_conceal_null       = "ø"
let g:javascript_conceal_this       = "@"
let g:javascript_conceal_return     = "⇚"
let g:javascript_conceal_undefined  = "¿"
let g:javascript_conceal_NaN        = "ℕ"
let g:javascript_conceal_prototype  = "¶"
let g:javascript_conceal_static     = "•"
let g:javascript_conceal_super      = "Ω"

" golang stuff
" if govet isn't working, try doing a `gometalinter --install` from the terminal
let g:syntastic_go_checkers = ['gometalinter']
let g:syntastic_go_gometalinter_args = '--aggregate'
let g:go_disable_autoinstall = 0
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_fmt_command = 'goimports'


augroup markdown_group
    au!
    autocmd BufEnter,BufNewFile,BufFilePre,BufRead *.md
        \ set filetype=markdown
        \| setlocal tabstop=4
        \| setlocal softtabstop=4
        \| setlocal shiftwidth=4 expandtab
        \| setlocal nowrap
augroup END

augroup mail_group
    au!
    autocmd FileType mail noremap <leader>d Odone :D<Esc>:wq<Return>
    autocmd FileType mail noremap <leader>a Oadded to schedule :D<Esc>:wq<Return>
augroup END

" need this line to be before loading colorscheme
" TODO: make this colorscheme stay after :w
" autocmd ColorScheme * highlight CohoHighlight ctermbg=DarkRed guibg=red
" highlight tabs; needs to be before color scheme is set

colorscheme desert
" desert darkblue default peachpuff
highlight Search ctermbg=Green ctermfg=Black
highlight Cursor ctermbg=Green ctermfg=Red
set cursorline
highlight CursorLine ctermbg=Black ctermfg=Yellow

set guifont=Monospace\ 12


" extra stuff"
" remaps q to bd so that buffer is also closed when quitting file
" cnoreabbrev wq w<bar>bd
" cnoreabbrev q bd

" remaps ; to : for easier commands during normal and insert phase
" already remapped in autohotkey"
" "nnoremap ; :
" "inoremap ; :
" "cnoremap ; :
" "vnoremap ; :
" "nnoremap : ;
" "inoremap : ;
" "cnoremap : ;
" "vnoremap : ;

