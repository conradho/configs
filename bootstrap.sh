#!/usr/bin/env bash
# script to run to bootstrap a completely new machine

set -o nounset -o errexit -o pipefail
IFS=$'\n\t'

pip3 install --user "ansible>=2.6"  # ansible with python3
export PATH=~/.local/bin:$PATH  # ansible-playbook is installed into here
export ANSIBLE_FORCE_COLOR=true
# time ansible-pull -d ~/configs -U https://bitbucket.org/conradho/configs.git ansible_automation/initial_setup_playbook.yml -i ansible_automation/hosts_inventory --accept-host-key

if [[ ! -d ~/configs ]]; then
    time git clone --shallow-submodules --recurse-submodules https://bitbucket.org/conradho/configs.git
else
    cd ~/configs
    time git pull
fi

# still need inventory here because .ansible.cfg not in homedir yet
echo "about to run"
read -p "time ansible-playbook ~/configs/ansible_automation/brand_new_machine_setup_playbook.yml -i ~/configs/ansible_automation/hosts_inventory --limit localhost"
time ansible-playbook ~/configs/ansible_automation/brand_new_machine_setup_playbook.yml -i ~/configs/ansible_automation/hosts_inventory --limit localhost
