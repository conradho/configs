### TODO
- mac install:
    - we do not do a vf install from within fish
    - mac now defaults to zsh instead of bash-- need to port some environmental exports etc over?
    - python install doesn't put python libs into bin, need a set PATH $PATH /Users/noob/Library/Python/3.8/bin
        - and macs don't use ':', instead ' '
    - our python is not the most updated?
- best practices
    - get ctags working for exuberant tags vs universal tags
        - or build universal tags for everyone
        - probably just take out the tag building step, or have a separate ansible step that does two diff commands
            - exuberant:  ctags -f ~/configs/tags ~/configs/
                          ctags --options=$HOME/configs/hi.ctags  -f ~/configs/tags ~/configs/
              coz if it's just a `.ctags` file then exuberant "can't find it"/only wants to accept a directory
        - also need to get tags to avoid third party git repos (in .vim)
    - setup CI
        - add compilation of mutt to ansible scripts
        - add ctags install / also run a a `ctags -R ~/configs` after install
          (but current vim checks already do that)
    - add systemd stuff to ansible?
    - update mac patch and make a pythonanywhere patch?
    - for the pa/configs aliases, only deactivate/workon if already in virtualenv/virtualenv exists
    - need to install virtualenvwrapper using default python, not python3
- housekeeping
    - if ~/log doesn't exist, then sendmail fails to write to ~/log/sendmail.log
    - fix pip3 problem on ubuntu where only v9.0.1 etc works and v10 fails
    - reorganize folders
        - chg ansible into (1) brand new setup (2) package installs & updates (3) dotfiles/symlinky (4) checks
    - update ansible master keys
    - workspace_housekeeping/tasks/clear_tmp.yml `lookup('env', 'SSH_AGENT_ENV_VAR_SCRIPT')` should be taken out if not initialized
- low priority
    - make chroot tests
    - convert README to org-mode file? https://github.com/jceb/vim-orgmode
    - use dotenv for passwords etc
    - turn sendmail into a submodule so you can use it elsewhere?

### Notes
- new way to add vim submodules:
- to move submodules:
    1. git submodule deinit .vim/bundle/ctrlp.vim
    2. git rm .vim/bundle/ctrlp.vim
    3. git submodule add https://github.com/scrooloose/syntastic.git dotfiles/.vim/pack/conrad/start/syntastic
        - or the old location git submodule add https://github.com/scrooloose/syntastic.git dotfiles/.vim/bundle/syntastic
- to update submodules:
    - `git submodule update --recursive --remote`
- to setup environment variables:
    1. just run `execute_with_environment_variables.py` for an interactive nested bash shell. be sure to exit after
    2. run it with a script passed in (`--command script1`)
    3. import it from within python
    4. pass in specific env var files (`--env env_var_file`)
- to run tests, just `python3 -m unittest`
    1. for more specific runs: `python3.5 -m unittest discover -s project_directory -p '*_test.py'`

- to manage mac vs linux discrepancies
    - on mac:
        - `git apply --check -v mac_patches/*`, then `git apply -v mac_patches/*`
        - can try to apply -3way or -reject to get conflicted files and then resolve them normally
        - then you need to git add individual files/lines of code, or `git apply -R mac_patches/*` when committing to git
    - to debug failed patch: `patch -p1 < failing.patch`
    - to recreate mac patches in case it starts conflicting because areas within the patch was changed:
        1. on mac, stay with mac branch and pull down/merge newest changes. resolve all conflicts
        2. now do a `git diff --patch > mac_patches/0001-mac-specific-changes.patch` 
        3. check that you can apply and revert
        4. push the patch

- to bootstrap:
    - `apt-get install git python python3 python-pip python3-dev python3-pip curl libssl-dev libncurses5-dev libncursesw5-dev python3-venv`
        - make sure default python version == default pip python version / remove any offending pips; but ansible will already `sudo -H pip install --upgrade pip` and also
    - in addition, in the build step:
        - to get vim compiled and tags working `apt install libncurses5-dev libncursesw5-dev exuberant-ctags`
        - to get mutt `apt install autoconf libtokyocabinet-dev`
        - to get scid vs pc `apt install tcl tk tcl-dev tk-dev`
    - need to have ruby installed `apt install ruby-full`
    - other useful apt installs
        - `apt install tmux fish silversearcher-ag lynx expect offlineimap3 notmuch imapfilter iftop uuid-dev libacl1-dev liblzo2-dev libxml2-dev libxslt1-dev libmysqlclient-dev libpq-dev libsqlite3-dev libffi-dev`
        - for gui environment: `apt install xclip chromium-browser keepassxc linux-headers-generic virtualbox-dkms virtualbox virtualbox-ext-pack`
    - `bash -c "$(curl https://bitbucket.org/conradho/configs/raw/master/bootstrap.sh)"`
    - note: the standard `curl api | bash` doesn't work because piping vs tty stuff when it tries to do an ansible pause prompt
- to setup ansible:
    - on a mac, need to enable SystemPreferences -> Sharing -> RemoteLogin
    - need to add `cat id_rsa.pub >> authorized_keys`
    - a quick test `ansible all -m copy -a "src=~/hi dest=~/configs/hi" -i <path-to-your-hosts-file>`
    - then run `ansible-playbook <path-to-your-yml-playbook> -i <path-to-your-hosts-file>`
    - to test only a single part, use tags: debug in the task, and run ansible-playbook with --tags debug
    - to limit to a single host, `--limit localhost`
- to compile/build your own vim:
    - `git clone https://github.com/vim/vim.git`, go into it and checkout branch
    - run this with the correct python config dirs `./configure --prefix ~/.local --with-features=huge --enable-multibyte --enable-python3interp=yes --enable-rubyinterp=yes --enable-luainterp=yes --with-compiledby=Conrad`
        - python-config and python3-config should be executables on your path
        - python3-config has --cflags and --ldflags options which you can reference and use in environment variable of ./configure command to specify python locations
        - deprecated but potentially useful is --with-python3-config-dir=$(python3-config --configdir)
            - the python2 version doesn't have a --configdir option, so can't shell out to find it
            - example python config dir: /usr/lib/python3.6/config-3.6m-x86_64-linux-gnu/
        - on a mac:
            - note: if both python2 and python3 enabled, then pythons must be loaded dynamically. so if mac python3 is in a weird directory, and the build flags to specify the directy aren't passed/used, then vim can't find python3
            - so just take out `--enable-pythoninterp=yes` and `--with-python-config-dir` to at least get a correctly compiled/linked vim first
            - if there are any packages that require python2 (doesn't seem like it), then can deal with loading both pythons dynamically and specifying python3 locations
    - make && make install
    - if you have failed previously, after installing relevant libraries etc, you may need to:
        - make clean
        - make distclean
        - make reconfig
- to compile/build own mutt:
    - you need apt-get install autoconf libtokyocabinet-dev
        - libtokyocabinet-dev for header caching
    - `git clone https://gitlab.com/muttmua/mutt.git`, go into it and checkout branch
    - `autoreconf -i; autoconf; automake --add-missing`
        - https://robots.thoughtbot.com/the-magic-behind-configure-make-make-install
    - do a `./configure --help` to get configure options
    - `./configure --prefix ~/.local --enable-sidebar --with-ssl --enable-imap --enable-hcache`
    - make && make install
        - if you have had prior makes with diff configs etc, you need to do a `make clean`
- offlineimap notes
    - if you need to rename folders/delete and resync a mail repo that already exists
        - setup the nametrans in imaprc
        - mv/delete the Maildir stuff (eg: ~/Mail/MyEmailRepo)
        - rename/delete the cache (otherwise remote will get deleted)
            - ie there are 3 files in ~/.offlineimap/, corresponding to your offlineimaprc settings account, localrepo and remoterepo
            - if you are renaming you will need to go in to fix everything
        - do a dryrun with `execute_with_environment_variables.py -e mutt.env --command="offlineimap -o --dry-run -a MyAccount"`
            - this only works if you have all the folders renamed/created, otherwise will error
        - remember to set createfolders to True during the first offlineimap sync
