---
- name: "symlinks | Wipe the backup folder"
  file: path=~/backups state=absent

- name: "symlinks | Recreate a new backup folder"
  file: dest=~/backups state=directory

# the alternative approach is to use with_lines
# which runs the command locally on the control machine
# not sure whether to base file list off remote or control
# .config folder has a whole bunch of other stuff in addition to
# what we are going to remove and symlink
- name: "symlinks | Generate a list of dotfiles excluding .config"
  shell: ls -A1 {{ configs_path }}/dotfiles | grep -v .config
  register: dotfiles_list

- name: "symlinks | Move old homedir files to backup folder"
  command: mv ~/{{ item }} ~/backups/{{ item }}
  # remote homedir may not have those files
  ignore_errors: yes
  with_items:
  - "{{ dotfiles_list.stdout_lines }}"
  # also .gitconfig, which is in the root level of configs repo
  - [ '.gitconfig' ]

- name: "symlinks | Move any old fish functions to backup folder"
  command: mv ~/.config/fish ~/backups/fish_stuff
  # remote homedir may not have those files
  ignore_errors: yes

- name: "symlinks | Create dotfile symlinks in homedir"
  file: src={{ configs_path }}/dotfiles/{{ item }}
        dest=~/{{ item }}
        state=link
  with_items: "{{ dotfiles_list.stdout_lines }}"

- name: "symlinks | Create .gitconfig symlink in homedir"
  file: src={{ configs_path }}/.gitconfig dest=~/.gitconfig state=link


- name: "symlinks | Make sure a .config folder exists"
  file: dest=~/.config state=directory

- name: "symlinks | Create .config/fish symlink in homedir"
  file: src={{ configs_path }}/dotfiles/.config/fish dest=~/.config/fish state=link force=yes

- name: "symlinks | Create .config/flake8 symlink in homedir"
  file: src={{ configs_path }}/dotfiles/.config/flake8 dest=~/.config/flake8 state=link force=yes

- name: "symlinks | Create .config/systemd symlink in homedir"
  file: src={{ configs_path }}/dotfiles/.config/systemd dest=~/.config/systemd state=link force=yes

- name: "symlinks | Make sure there is a .local/bin directory"
  file: dest=~/.local/bin state=directory

- name: "symlinks | Symlink bin file onto path"
  file:
    src: "{{ configs_path }}/utils_bin/{{ item }}"
    dest: ~/.local/bin/{{ item }}
    state: link
  with_items: [ 'flip', 'goto_pythonanywhere.py', 'execute_with_environment_variables.py', 'sendmail', 'diff-highlight' ]
