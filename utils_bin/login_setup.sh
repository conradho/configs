#!/usr/bin/env bash

# hack where gnome desktop manager really breaks xmodmap
# also breaks when u switch keyboard/input methods
# https://bugzilla.gnome.org/show_bug.cgi?id=721873
# https://bugs.launchpad.net/ubuntu/+bug/1243642
sleep 10
/usr/bin/xmodmap ~/.Xmodmap
