#!/usr/bin/env python3.5

from collections import namedtuple
import io
import json
import os
import shutil
import shlex
import subprocess
import tempfile
from textwrap import dedent
from unittest import TestCase
from unittest.mock import patch

from simplecrypt import encrypt

from utils_bin.execute_with_environment_variables import (
    setup_base_env_json,
    get_file_path_magic,
    set_env_vars_from_io_stream,
    process_env_var_file_and_password_tuples,
    BASE_ENV_VAR_FILE,
    get_args,
    execute,
)

TempFileTuple = namedtuple('TemporaryFile', ['file_descriptor', 'path'])

class FakeFolderMixin():
    @classmethod
    def setUpClass(cls):
        cls.mock_env_var_folder = tempfile.mkdtemp()
        cls.patcher = patch(
            'utils_bin.execute_with_environment_variables.ENV_VAR_FOLDER_PATH',
            cls.mock_env_var_folder
        )
        cls.patcher.start()
        # don't put stop() in tearDown because setUp may error
        ## self.addCleanup(patcher.stop)

        setup_base_env_json()

    @classmethod
    def tearDownClass(cls):
        cls.patcher.stop()

class SetupBaseEnvironmentVariablesTest(FakeFolderMixin, TestCase):

    def test_get_base_env_var_dict_returns_correct_values(self):
        with open(os.path.join(self.mock_env_var_folder, BASE_ENV_VAR_FILE), 'r') as json_file:
            env_var_dict = json.load(json_file)

        username = subprocess.check_output('whoami')
        username_unicode = username.decode('UTF-8').strip()
        self.assertEqual(env_var_dict['USERNAME'], username_unicode)
        possible_home_dirs = [
            os.path.join('/home', username_unicode),
            # coz macs are stupid
            os.path.join('/Users', username_unicode),
        ]

        self.assertIn(env_var_dict['HOME'], possible_home_dirs)

class GetFilePathMagicTest(TestCase):

    def setUp(self):
        self.opened_files = []

        # make a file within env var folder
        self.mock_env_var_folder = tempfile.mkdtemp()
        opened_file, self.mock_file_in_env_var_folder = tempfile.mkstemp(dir=self.mock_env_var_folder)
        self.opened_files.append(opened_file)

        # make a file inside a folder of cwd
        self.mock_cwd_folder = tempfile.mkdtemp(dir=os.getcwd())
        opened_file, self.mock_file_in_cwd_folder = tempfile.mkstemp(dir=self.mock_cwd_folder)
        self.opened_files.append(opened_file)

    def tearDown(self):
        for opened_file in self.opened_files:
            os.close(opened_file)
        ## os.remove(self.mock_file)
        shutil.rmtree(self.mock_env_var_folder, ignore_errors=True)
        shutil.rmtree(self.mock_cwd_folder, ignore_errors=True)

    def test_absolute_path_works(self):
        self.assertEqual(
            get_file_path_magic(self.mock_file_in_cwd_folder),
            self.mock_file_in_cwd_folder
        )

    def test_relative_path_to_cwd_works(self):
        rel_path = os.path.relpath(self.mock_file_in_cwd_folder)
        self.assertEqual(
            get_file_path_magic(rel_path),
            os.path.abspath(self.mock_file_in_cwd_folder)
        )

    def test_file_name_in_env_var_folder_works(self):
        with patch('utils_bin.execute_with_environment_variables.ENV_VAR_FOLDER_PATH', self.mock_env_var_folder):
            file_name = os.path.basename(self.mock_file_in_env_var_folder)
            self.assertEqual(
                get_file_path_magic(file_name),
                self.mock_file_in_env_var_folder
            )

    def test_wrong_path_errors(self):
        wrong_path = '/abc'
        with patch('utils_bin.execute_with_environment_variables.sys.stdout') as mock_stdout:
            with self.assertRaises(SystemExit) as mgr:
                get_file_path_magic(wrong_path)

        mock_stdout.write.assert_any_call(
            'Environment variables file {} was not found.'.format(wrong_path)
        )
        self.assertEqual(mgr.exception.code, -1)


class SetEnvVarsFromIoStreamTest(TestCase):

    def test_non_dict_json_errors_nicely(self):
        with io.StringIO("some initial text data") as random_stream:
            with self.assertRaises(SystemExit) as mgr:
                with patch('sys.stdout', new=io.StringIO()) as mock_stdout:
                    set_env_vars_from_io_stream(random_stream)

        self.assertIn('not formatted correctly', mock_stdout.getvalue())
        self.assertEqual(mgr.exception.code, -1)

    def test_can_access_env_var(self):
        with io.StringIO(json.dumps({'abc': 123})) as json_stream:
            set_env_vars_from_io_stream(json_stream)
            # 123 is converted to '123'
        self.assertEqual(os.environ['abc'], '123')

    def test_env_var_key_and_value_are_turned_into_str(self):
        with io.StringIO(json.dumps({55555.0: 123})) as json_stream:
            set_env_vars_from_io_stream(json_stream)
        self.assertEqual(os.environ['55555.0'], '123')

    def test_env_var_does_overwrite_existing_one(self):
        with io.StringIO(json.dumps({'abc': 123})) as json_stream:
            set_env_vars_from_io_stream(json_stream)
        self.assertEqual(os.environ['abc'], '123')
        with io.StringIO(json.dumps({'abc': 567})) as other_stream:
            set_env_vars_from_io_stream(other_stream)
        self.assertEqual(os.environ['abc'], '567')

class ProcessEnvVarFileAndPasswordTuplesTest(FakeFolderMixin, TestCase):

    def setUp(self):
        self.temp_files = {}
        names = ['additional_file', 'additional_file_2']
        write_messages = ['writing in {}'.format(name) for name in names]
        for name, msg in zip(names, write_messages):
            temp_file = TempFileTuple(*tempfile.mkstemp())
            with os.fdopen(temp_file.file_descriptor, 'w') as opened_file:
                opened_file.write(msg)
                # this also closes the whole file; don't need to os.close it
            self.temp_files[name] = temp_file

    def tearDown(self):
        for temp_file in self.temp_files.values():
            os.remove(temp_file.path)

    @patch('utils_bin.execute_with_environment_variables.set_env_vars_from_io_stream')
    def test_base_file_with_correct_path_is_processed(self, mock_set_env_vars_from_io_stream):
        # also problems if base file uses file_path_magic to get path
        process_env_var_file_and_password_tuples()
        self.assertEqual(mock_set_env_vars_from_io_stream.call_count, 1)

        # use assertIn instead of assertEqual because of file_path_magic
        # this test will fail if BASE_ENV_VAR_FILE is not set correctly
        self.assertIn(
            BASE_ENV_VAR_FILE,
            mock_set_env_vars_from_io_stream.call_args[0][0].name
            # of the call args, look at the first positional argument [0][0]
            # it is a file stream. get it's name (path)
        )

    @patch('utils_bin.execute_with_environment_variables.set_env_vars_from_io_stream')
    def test_can_pass_multiple_files(self, mock_set_env_vars_from_io_stream):
        process_env_var_file_and_password_tuples([
            (self.temp_files['additional_file'].path, None),
            (self.temp_files['additional_file_2'].path, None),
        ])

        self.assertIn(
            BASE_ENV_VAR_FILE,
            mock_set_env_vars_from_io_stream.call_args_list[0][0][0].name
        )
        self.assertEqual(
            self.temp_files['additional_file'].path,
            mock_set_env_vars_from_io_stream.call_args_list[1][0][0].name
        )
        self.assertEqual(
            self.temp_files['additional_file_2'].path,
            mock_set_env_vars_from_io_stream.call_args_list[2][0][0].name
        )

    def test_can_decrypt_files(self):
        with open(self.temp_files['additional_file'].path, 'wb') as f:
            f.write(encrypt('pw123', 'super secret message'))

        def print_io_stream(file_stream):
            print(file_stream.read())  # .decode('UTF-8')

        with patch('utils_bin.execute_with_environment_variables.set_env_vars_from_io_stream', new=print_io_stream):
            with patch('sys.stdout', new=io.StringIO()) as mock_stdout:
                process_env_var_file_and_password_tuples([
                    (self.temp_files['additional_file'].path, 'pw123'),
                ])
        # mock_stdout gets passed two files: base file and encrypted
        self.assertIn('super secret message\n', mock_stdout.getvalue())


class GetArgsTest(TestCase):

    def test_can_parse_just_command(self):
        command, files = get_args(shlex.split('--command "echo hi"'))
        self.assertEqual(command, 'echo hi')
        self.assertEqual(files, [])

    def test_can_parse_one_file(self):
        command, files = get_args(shlex.split('--env_var_file "abc.txt"'))
        self.assertEqual(command, None)
        self.assertEqual(files, [['abc.txt', None]])

    def test_can_parse_multiple_files(self):
        command, files = get_args(shlex.split('--env_var_file "abc.txt" --env_var_file "def.txt"'))
        self.assertEqual(command, None)
        self.assertEqual(files, [['abc.txt', None], ['def.txt', None]])

    def test_can_parse_encrypted_file(self):
        command, files = get_args(shlex.split('--env_var_file "abc.txt" secret_password'))
        self.assertEqual(command, None)
        self.assertEqual(files, [['abc.txt', 'secret_password']])


class ExecuteWithEnvironmentVariablesTest(FakeFolderMixin, TestCase):

    def test_gives_interactive_bash_with_bashrc_loaded_if_no_command_passed(self):
        ## with patch(
        output = execute(files_and_passwords=[], command='echo hi')
        print(output)

    def test_passes_sigterm_correctly(self):
        python_script = dedent(
            """
            import time
            try:
                while True:
                    time.sleep(1)
            except KeyboardInterrupt:
                print('oh. received ctrl-c, let me handle it')
            """
        )
        # https://docs.python.org/3.5/library/signal.html#example
        # set an alarm that will send a sigkill to this process
        # make sure that the inner python script receives it?

        # execute(files_and_passwords=[], command='python -c "{}"'.format(python_script))
