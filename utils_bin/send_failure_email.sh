#!/bin/bash

$HOME/.local/bin/sendmail conrad.alwin.ho@gmail.com <<ERROR_EMAIL
To: conrad.alwin.ho@gmail.com
From: conrad@pythonanywhere.com
Subject: systemd service [$1]
Content-Tyhpe: text/plain

systemd running on $USER@$HOSTNAME triggered an email on `date --utc`
ERROR_EMAIL
