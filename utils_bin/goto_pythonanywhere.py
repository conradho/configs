#!/usr/bin/python
import subprocess
import sys

def get_raw_query():
    try:
        main_clipboard = subprocess.check_output(['xclip', '-selection', 'clipboard', '-o']).strip()
    except subprocess.CalledProcessError:
        main_clipboard = ''
    try:
        selection_clipboard = subprocess.check_output(['xclip', '-selection', 'primary', '-o']).strip()
    except subprocess.CalledProcessError:
        selection_clipboard = ''

    if '@' in main_clipboard and '@' not in selection_clipboard:
        clipboard = main_clipboard
    else:
        clipboard = selection_clipboard
    return clipboard

def get_correct_query():
    # should do the raw_input thing where if copy'd something wrong, will ask them to copy again or manually input
    return get_raw_query()

def go_to_website(user_name, website_part):
    URLS = {
        'admin': 'https://www.pythonanywhere.com/admin/auth/user/?q={}',
        'console': 'https://www.pythonanywhere.com/user/{}/consoles/',
        'forum': 'https://www.pythonanywhere.com/forums/'
    }
    target = URLS[website_part].format(user_name)

    process = subprocess.Popen(['firefox', target])
    sys.exit(process.wait())

if __name__ == '__main__':
    query = get_correct_query()
    _, website_part = sys.argv
    go_to_website(query, website_part)
