#!/usr/bin/env python
"""
Idea 1:
- undersample to take an equal number of spam and nonspam emails
- do a bag of words with TF-IDF (so "the" doesn't overpower- but we need some precomputed IDF)
- use cosine similarity to compare to a couple spam samples
- if close enough then spam
"""

import email
from email import policy
from pathlib import Path

import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

# TfidfVectorizer is equivalent to CountVectorizer followed by TfidfTransformer.
# CountVectorizer takes a list of text documents and gives a matrix of token counts
# TfidfTransformer takes a count matrix and gives a normalized tf or tf-idf representation


def get_messages():
    for email_path in Path("~/Mail/Spam/cur").expanduser().iterdir():
        with open(email_path, encoding="utf-8", errors="replace") as f:
            # need the policy.default otherwise get_body doesn't exist
            body = email.message_from_file(f, policy=policy.default).get_body(
                preferencelist=("plain", "html", "related")
            )
            if body is not None:
                yield body.get_content()


def make_bag_of_words(list_of_documents):
    # if we set something like max_features=10, some emails may just have
    # similarity = 0 with everyone if all their words are not selected as model
    # features
    vectorizer = TfidfVectorizer()
    # fit() generates the idf from the test data, and updates the vectorizer to use it
    # transform() takes one (or more) documents and give its tfidf representation for each document
    # fit_transform calls fit() and then returns transform() on the documents
    transformed = vectorizer.fit_transform(list_of_documents)
    # we also return the transformed documents, so that we can later compare
    # test emails to the training data to find the most similar document
    return vectorizer, transformed


def get_most_similar_email(new_email, pretrained_model, available_comparisons):
    transformed = pretrained_model.transform([new_email])
    similarity = pd.DataFrame(cosine_similarity(transformed, available_comparisons))
    # rotate it to be a column and then squeeze it into a Series
    column_similarity = similarity.T.squeeze()
    return column_similarity.idxmax(), column_similarity.max()


if __name__ == "__main__":
    messages = list(get_messages())
    model, transformed_training_set = make_bag_of_words(messages)
    # these are the tokens we will keep track of
    # print(model.get_feature_names())
    most_similar_id, score = get_most_similar_email(messages[1], model, transformed_training_set)
    print("most similar email (with a score of {:3f}):".format(score))
    print("=" * 80 + "\n", messages[most_similar_id][:100], "\n" + "=" * 80)
