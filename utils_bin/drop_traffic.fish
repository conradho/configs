#!/usr/bin/fish


# this was always kinda flakey at best. replaced with browser extension-- BlockSite

# argparse takes a string (eg: $argv) and removes options and sets them
# as local variables, so only positional arguments remain from argv
set --local options "h/help" "u/undo"
argparse --name=drop_traffic.fish $options -- $argv


set --local sites facebook.com youtube.com twitter.com instagram.com

if set --query _flag_help
    echo help called, early exit
    exit
end

if set --query _flag_undo
    for site in $sites
        echo unblocking $site
        /sbin/iptables -D INPUT -s $site -j DROP
        /sbin/iptables -D OUTPUT -d $site -j REJECT
        /sbin/iptables -D INPUT -t filter -m string --string $site -j LOG --algo bm
        /sbin/iptables -D INPUT -t filter -m string --string $site -j REJECT --algo bm
    end
else 
    for site in $sites
        echo blocking $site
        /sbin/iptables -I INPUT -s $site -j DROP
        /sbin/iptables -I OUTPUT -d $site -j REJECT
        /sbin/iptables -I INPUT -t filter -m string --string $site -j LOG --algo bm
        /sbin/iptables -I INPUT -t filter -m string --string $site -j REJECT --algo bm
    end
end


