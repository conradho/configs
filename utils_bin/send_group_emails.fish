#!/usr/bin/fish

# argparse takes a string (eg: $argv) and removes options and sets them
# as local variables, so only positional arguments remain from argv
set --local options "h/help" "t/template_path="
argparse --name=send_group_emails.fish $options -- $argv

if set --query _flag_help
    set --show _flag_help
    echo help called, early exit
    exit
end

if not test -f $_flag_template_path
    echo no template found
    exit
end
set --local email_addresses $argv
if not test -f $email_addresses
    echo email_addresses file not found
    exit
end

for email_address in (cat $email_addresses)
    if test -z $email_address
        continue
    end
    echo sending out $email_address
    echo "To: $email_address" | cat - $_flag_template_path | execute_with_environment_variables.py -e mutt.env --command "sendmail $email_address agata.brajdic@gmail.com"
end


exit

# or instead of piping with | before the sendmail command, could do
# sendmail < (echo to_addr | cat - template | psub)
# psub puts into temporary pipe and returns file name
# note "<" doesn't work without psub because "<" expects a file path that it then passes file content into stdin, but not the content itself
