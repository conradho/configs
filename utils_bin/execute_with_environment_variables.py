#!/usr/bin/env python3

import argparse
import io
import json
import os
import pwd
import shlex
import subprocess
import sys
from datetime import datetime

from simplecrypt import decrypt

# realpath deals with symlink and returns an absolute path
ENV_VAR_FOLDER_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "env_var_files")
BASE_ENV_VAR_FILE = "base.env"


def setup_base_env_json():
    env_var_dict = {}
    env_var_dict["USERNAME"] = pwd.getpwuid(os.getuid())[0]
    env_var_dict["HOME"] = os.path.expanduser("~")
    assert env_var_dict["USERNAME"] in env_var_dict["HOME"]
    env_var_dict["SECRET"] = "lalala"

    if not os.path.exists(ENV_VAR_FOLDER_PATH):
        os.makedirs(ENV_VAR_FOLDER_PATH)

    with open(os.path.join(ENV_VAR_FOLDER_PATH, BASE_ENV_VAR_FILE), "w") as base_env_var_file:
        json.dump(env_var_dict, base_env_var_file, indent=4)


def get_file_path_magic(file_path):
    if not os.path.isfile(file_path):
        try_file_path = os.path.join(ENV_VAR_FOLDER_PATH, file_path)
        if os.path.isfile(try_file_path):
            file_path = try_file_path
        else:
            print("Environment variables file {} was not found.".format(file_path))
            sys.exit(-1)
    return os.path.abspath(file_path)


def set_env_vars_from_io_stream(io_stream):
    try:
        env_var_dict = json.load(io_stream)
        assert type(env_var_dict) == dict
    except Exception:
        print("Environment variables json was not formatted correctly.")
        sys.exit(-1)
    for var_name, var_value in env_var_dict.items():
        # os env vars must be strings
        ## os.environ.setdefault(str(var_name), str(var_value))
        os.environ[str(var_name)] = str(var_value)


def process_env_var_file_and_password_tuples(additional_file_pw_pairs=[]):
    list_of_tuples = [(BASE_ENV_VAR_FILE, None)] + additional_file_pw_pairs
    for env_var_file, decryption_pw in list_of_tuples:
        file_path = get_file_path_magic(env_var_file)
        if decryption_pw is None:
            with open(file_path, "r") as file_stream:
                set_env_vars_from_io_stream(file_stream)
        else:
            with open(file_path, "rb") as file_stream:
                encrypted_bytes = file_stream.read()
                unencrypted_bytes = decrypt(decryption_pw, encrypted_bytes)
                set_env_vars_from_io_stream(io.StringIO(unencrypted_bytes.decode("UTF-8")))


def execute(files_and_passwords, command):
    """
    for a normal bash command, notice the double quotes

            >> bash -c "echo $HOME"

    subprocess.call() doesn't need the double quotes otherwise it errors

            >> subprocess.call(['bash', '-c', 'echo $testing'])

    instead of

            >> subprocess.call(['bash', '-c', '"echo $testing"'])

    shlex.split() takes care of all this for us.

    all subprocesses have access to the environ variable
    """
    process_env_var_file_and_password_tuples(files_and_passwords)
    command = command if command else "bash -i"
    proc = subprocess.Popen(shlex.split(command))
    while proc.poll() is None:
        try:
            proc.wait()
        except KeyboardInterrupt:
            # ctrl+c is automatically already passed along to the child
            # sending a sigkill/sigterm will just kill the child
            pass
    print("Done at {}".format(datetime.utcnow()))


def get_args(override_sys_argv=None):
    parser = argparse.ArgumentParser(
        description="Sets up environment so that commands execute with environment variables",
        prog="Environment Variable Setup Program",
    )
    parser.add_argument("--init", help="use this flag to initialize base.env", action="store_true")
    parser.add_argument("--command", type=str, help="command to be executed", default=None)
    parser.add_argument(
        "-e",
        "--env_var_file",
        type=str,
        help="any environment variable file to pass in",
        action="append",  # pass in more then one file
        nargs="+",  # each single flag can pass in file + password
    )

    if override_sys_argv is None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(override_sys_argv)

    if args.init:
        setup_base_env_json()

    if args.env_var_file is None:
        args.env_var_file = []
    else:
        for file_pw_tuple in args.env_var_file:
            if len(file_pw_tuple) == 1:
                file_pw_tuple.append(None)
            elif len(file_pw_tuple) != 2:
                raise Exception("Environment variable file option has too many parameters")
    return args.command, args.env_var_file


def main():
    ## execute_with_environment_variables.py --command "bash -c \"echo \$HOME\""
    ## execute_with_environment_variables.py --env_var_file mutt.env --command mutt
    ## from execute_with_environment_variables import process_env_var_file_and_password_tuples
    command, env_var_files_and_passwords = get_args()
    print(execute(env_var_files_and_passwords, command))


if __name__ == "__main__":
    main()
