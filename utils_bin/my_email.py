import os
import smtplib
from collections import namedtuple
from contextlib import contextmanager

EmailItem = namedtuple("EmailItem", "recipients, message")


# the same as a context manager object with an __init__, __entry__, __exit__ functions
@contextmanager
def smtp_email_server():
    # get the environment variables inside this function so other places
    # importing this module doesn't error
    smtp_server = os.environ["REMOTE_SERVER"]
    smtp_port = os.environ["SMTP_URL"].split(":")[-1]
    smtp_user = os.environ["USER"]
    smtp_password = os.environ["PASSWORD"]

    # need longer timeout for say an attachment that is being sent
    server = smtplib.SMTP(smtp_server, smtp_port, timeout=60)
    server.starttls()
    server.ehlo()  # after starting tls
    server.login(smtp_user, smtp_password)
    yield server
    server.close()


def send_email(email_item):
    with smtp_email_server() as server:
        server.send_message(msg=email_item.message, to_addrs=email_item.recipients)
