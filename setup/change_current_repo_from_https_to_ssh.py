#!/usr/bin/env python3

import os
import subprocess
import shlex
import sys

WORKING_DIR = os.path.dirname(__file__)
command_to_get_url = "git config --get remote.origin.url"

process = subprocess.check_output(
    shlex.split(command_to_get_url),
    cwd=WORKING_DIR
)
old_url_byte = process
old_url = old_url_byte.decode('UTF-8')

print('Old remote url was {}.'.format(old_url))

if old_url.startswith('git'):
    # already uses an ssh key
    print('Did not need to change url')
    sys.exit(0)

split_url = old_url.split('/')
assert len(split_url) > 2
new_url = 'git@bitbucket.org:{repo_owner}/{repo_name}'.format(
    repo_owner=split_url[-2],
    repo_name=split_url[-1],
)


print('Setting new remote url {}.'.format(new_url))

command_to_set_url = 'git remote set-url origin {}'.format(new_url)
subprocess.call(
    shlex.split(command_to_set_url),
    cwd=WORKING_DIR
)
